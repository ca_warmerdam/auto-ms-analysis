#!/usr/bin/env python3
"""
Script containing a Sample class that can be used for scoring the sample
"""

import base64
import struct
import xml
import zlib
import sys
from decimal import Context

from collections import Counter

import numpy as np
from matplotlib import pyplot

__author__ = "C.A. (Robert) Warmerdam"
__credits__ = ["Robert Warmerdam"]
__status__ = "Development"

MZ_XML_SCAN_ELEMENT = './/{http://sashimi.sourceforge.net/schema_revision/mzXML_3.2}scan'
MZ_ERROR = 0.5
PLOT = False


class Sample:
    non_frequent_adduct_masses = [
        ("M+3H", 0.33, 1.007276),
        ("M+2H+Na", 0.33, 8.334590),
        ("M+H+2Na", 0.33, 15.766190),
        ("M+3Na", 0.33, 22.989218),
        ("M+2H", 0.5, 1.007276),
        ("M+H+NH4", 0.5, 9.520550),
        ("M+H+Na", 0.5, 11.998247),
        ("M+H+K", 0.5, 19.985217),
        ("M+ACN+2", 0.5, 21.520550),
        ("M+2Na", 0.5, 22.989218),
        ("M+2ACN+2", 0.5, 42.033823),
        ("M+3ACN+2", 0.5, 62.547097)
    ]
    frequent_adduct_masses = [
                              ("M+H", 1, 1.007276),
                              ("M+NH4", 1, 18.033823),
                              ("M+Na", 1, 22.989218),
                              ("M+CH3OH+H", 1, 33.033489),
                              ("M+K", 1, 38.963158),
                              ("M+ACN+H", 1, 42.033823),
                              ("M+2Na-H", 1, 44.971160),
                              ("M+IsoProp+H", 1, 61.065340),
                              ("M+ACN+Na", 1, 64.015765),
                              ("M+2K+H", 1, 76.919040),
                              ("M+DMSO+H", 1, 79.021220),
                              ("M+2ACN+H", 1, 83.060370),
                              ("M+IsoProp+Na+H", 1, 84.055110),
                              ("2M+H", 2, 1.007276),
                              ("2M+NH4", 2, 18.033823),
                              ("2M+Na", 2, 22.989218),
                              ("2M+3H2O+2H", 2, 28.023120),
                              ("2M+K", 2, 38.963158),
                              ("2M+ACN+H", 2, 42.033823),
                              ("2M+ACN+Na", 2, 64.015765)]
    ctx = Context(prec=10)

    def __init__(self, path, identifier, mol_weight, retention_time_range, mz_range):
        self.mass_array = None
        self.intensity_array = None
        self.data = Counter()
        self.spectrum_tree = None
        try:
            # Try to open the file and read it
            self.spectrum_tree = xml.etree.ElementTree.parse(path)
        except xml.etree.ElementTree.ParseError as e:
            raise Exception("{} in '{}'".format(str(e), path))
        self.retention_time_range = retention_time_range
        self.mol_weight = mol_weight
        self.identifier = identifier
        self.path = path
        self.mz_range = list(mz_range)
        # self.mz_range[0] = self.mol_weight
        self._prediction = 0

    @property
    def prediction(self):
        return self._prediction

    @staticmethod
    def _decode_peaks(content, d_array_length):
        """
        Decode peaks
        :param content: the encoded array
        :param d_array_length: amount of peaks
        :return: list of decoded values with alternating m/z value and intensity
        """
        # Decode base64 data
        decoded_data = base64.b64decode(content)

        # Decompress data
        uncompressed_data = zlib.decompress(decoded_data)

        # Unpack data
        data = struct.unpack('>{}d'.format(d_array_length * 2), uncompressed_data)
        return data

    def _get_frequent_adduct_masses(self):
        """
        Calculate masses with adduct entries.
        :return: masses sorted by distance between mol weight (ascending)
        """
        masses = [(self.mol_weight * multiplier + addition, iso) for iso, multiplier, addition in self.frequent_adduct_masses]
        ranked_masses = list()
        for i, mass in enumerate(sorted(masses, key=lambda m: abs(self.mol_weight-m[0]))):
            # Round mass as instructed
            ranked_masses.append((round(mass[0]), mass[1]))
        return ranked_masses

    def get_peak_spectrum(self):
        """
        Get the peak spectrum
        """
        mass_list = []
        intensity_list = []
        # Collect scans in the time range and with msLevel not 0
        for scan in self.spectrum_tree.findall(MZ_XML_SCAN_ELEMENT):

            # Make sure PT...  and ...S are stripped from retention time attribute
            # Divide by 60 to get seconds
            retention_time = float(scan.attrib['retentionTime'][2:-1]) / 60
            # Discard the scan when msLevel is 0 or when the scan is not within time range
            if scan.attrib['msLevel'] in ["0", ""] or \
                    not (self.retention_time_range[0] < retention_time < self.retention_time_range[1]):
                continue

            # Get the peaks
            peaks = scan.find('{http://sashimi.sourceforge.net/schema_revision/mzXML_3.2}peaks')
            d_array_length = int(scan.attrib['peaksCount'])
            # Decode the peaks
            peaks = self._decode_peaks(peaks.text, d_array_length)

            # Extend lists
            mass_list.extend(peaks[::2])
            intensity_list.extend(peaks[1::2])
        # Filter the NAN and infinity values from the peak spectrum
        mass_array = np.asarray(mass_list)
        intensity_array = np.asarray(intensity_list)
        indices = np.where(np.logical_or(np.isnan(mass_array), np.isinf(mass_array), np.isnan(intensity_array)))
        for index in indices:
            mass_array = np.delete(mass_array, index)
            intensity_array = np.delete(intensity_array, index)
        self.mass_array = mass_array
        self.intensity_array = intensity_array

    def process_mass_spectrum(self):
        """
        Process mass spectrum
        """
        mz_range = range(int(self.mz_range[0]), int(self.mz_range[1]))

        # Get inter quantile range
        q1 = np.nanquantile(self.intensity_array, 0.25)
        q3 = np.nanquantile(self.intensity_array, 0.75)
        iqr = q3 - q1

        for i, mz in enumerate(self.mass_array):
            # Filter out every intensity value below 0 or above quantile q3 + 5000 * inter quantile range
            if self.intensity_array[i] < 0 or self.intensity_array[i] > (q3 + 5000 * iqr):
                # Next
                continue
            for mz_value in mz_range:
                # Collect peaks with max 0.5 difference between integer MZ value
                if (mz_value - MZ_ERROR) <= mz <= (mz_value + MZ_ERROR):
                    self.data[mz_value] += self.intensity_array[i]

    def get_prediction(self):
        """
        Get the prediction of this sample
        """
        mz_list, int_list = tuple(zip(*list(sorted(self.data.items(), key=lambda x: x[0]))))

        # Plot if PLOT set to True
        if PLOT:
            self.plot_spectrum(int_list, mz_list)

        # Get frequent adduct masses to look for
        mz_s = self._get_frequent_adduct_masses()

        # Print important values
        print("Predicting: {}".format(self.identifier))
        print('exact mass:', self.mol_weight)
        # Get maximum intensity (base peak intensity)
        max_intensity = max(list(self.data.values()))
        print("highest peak:", mz_list[int_list.index(max_intensity)], max_intensity)

        # Initialize mz of final score
        final_mz = None
        for mz, ion_name in mz_s:
            # Get relative intensity
            intensity = self.data[mz]
            rel_intensity = intensity / max_intensity
            score = rel_intensity
            # Check if this scores better
            if score > self._prediction:
                self._prediction = score
                final_mz = mz, ion_name
        if final_mz is not None:
            print("{} Prediction: {} at M/Z {} ({})".format(self.identifier, self.prediction, *final_mz))

    def plot_spectrum(self, int_list, mz_list):
        """
        Method that plots the spectrum in a histogram, with m/z identifier above each bar
        :param int_list: The list of intensities
        :param mz_list: The list of mass-charge ratios
        """
        pyplot.title(self.identifier + ": " + str(self.mol_weight))
        rects = pyplot.bar(np.asarray(mz_list), int_list, 1)
        pyplot.xlabel("m/z")
        pyplot.ylabel("Intensity")
        for i, rect in enumerate(rects):
            # Get the height of the bar
            height = rect.get_height()
            # Plot the m/z above each bar
            pyplot.text(rect.get_x() + rect.get_width() / 2., 1.05 * height,
                        mz_list[i],
                        ha='center', va='bottom', fontsize=6)
        pyplot.show()


def main(argv=None):
    if argv is None:
        argv = sys.argv
    return 0


if __name__ == "__main__":
    sys.exit(main())
