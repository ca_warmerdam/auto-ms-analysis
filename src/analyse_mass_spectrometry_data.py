#!/usr/bin/env python3

"""
Script for classifying mass spectrometry mzXML files.

usage: analyse_mass_spectrometry_data.py [-h] -d DATA [DATA ...] -o
                                         OUTPUT_FILE -i
                                         SAMPLE_INFORMATION_FILE_PATH
                                         [-R RETENTION_TIME_RANGE RETENTION_TIME_RANGE]
                                         [-M MZ_RANGE MZ_RANGE]

Automatic mass spectrometry analyzer

optional arguments:
  -h, --help            show this help message and exit
  -d DATA [DATA ...], --data DATA [DATA ...]
                        Give a path for the directory where data is stored to
                        analyse. This can be multiple files. The asterisk can
                        be used as wildcard. The argument 'd:\files\*.mzxml'
                        will result in all mzxml files being analyzed located
                        inside the folder: 'd:\files'
  -o OUTPUT_FILE, --output_file OUTPUT_FILE
                        Give a file path the output files can be written to.
                        For example: 'd:\files\heatmap.xlsx'
  -i SAMPLE_INFORMATION_FILE_PATH, --sample_information_file_path SAMPLE_INFORMATION_FILE_PATH
                        Specify a smiles file with spaces as a separator that
                        contains a smiles in the first column and the location
                        on the output plate in the last column.
  -R RETENTION_TIME_RANGE RETENTION_TIME_RANGE, --retention_time_range RETENTION_TIME_RANGE RETENTION_TIME_RANGE
                        Specify the retention time to filter on. default is
                        between 0.2 and 0.5.
  -M MZ_RANGE MZ_RANGE, --mz_range MZ_RANGE MZ_RANGE
                        Specify the mass/charge range to filter on. default is
                        between 200 and 600.
"""

import sys
import warnings

import graphical_user_interface
import mass_spectrometry_heatmap_tool

import pandas as pd

from openbabel import pybel as pb

from argument_parser import ArgumentParser
from mass_spectrum_analyzer import MassSpectraAnalyzer

__author__ = "C.A. (Robert) Warmerdam"
__credits__ = ["Robert Warmerdam"]
__status__ = "Development"


def get_mol_weight(smiles):
    """
    Returns molecular weight given a smiles
    :param smiles: smiles molecule in str
    :return: molecular weight in float
    """
    mol = pb.readstring("smi", smiles)
    return mol.exactmass


def get_sample_information_table(sample_information_file_path):
    """
    Function processing the sample information file path to a pandas table.
    The file should have a spaces as a delimiter.
    :param sample_information_file_path:
    :return: numpy array containing at least 2 columns
    The output is as follows:
    First Column: Molecule in smiles
    Last Column: Location in the plate (A1, A2, B1, B2, etc.)
    """
    # Get the maximum column length
    cols = 0
    with open(sample_information_file_path) as f:
        for row in f:
            # Count the number of delimiters
            cols = max(cols, row.count(" ") + 1)

    # Create correct amount of names
    names = ['loc_' + str(i) for i in range(cols - 2)]
    names.insert(0, 'smiles')
    names.append('location')

    sample_information_table = pd.read_table(sample_information_file_path, sep=' ', header=None,
                                             names=names)
    # Get the last non nan value for every row to make sure the location column holds the last value
    sample_information_table['location'] = sample_information_table.ffill(axis=1).iloc[:, -1]
    # Add mol weights
    sample_information_table['mol_weight'] = sample_information_table.apply(lambda row: get_mol_weight(row['smiles']),
                                                                            axis=1)
    # Return table / array
    return sample_information_table


def get_label_data_table(label_data):
    """
    Function processing the label data file path to a pandas table.
    The file should have a commas as a delimiter.
    :param label_data:
    :return: numpy array containing at least 2 columns
    The output is as follows:
    First Column: Location in the plate (A1, A2, B1, B2, etc.)
    Second Column: label like 0, 0.5 or 1
    """
    # Convert file to the data frame
    label_data_table = pd.read_table(label_data, sep=',', header=None, dtype={'location': str, 'label': str},
                                     names=['location', 'label'], usecols=[0, 1])

    # Strip labels from spaces
    label_data_table = label_data_table.apply(lambda x: x.str.strip() if x.dtype == "object" else x)

    # Return table / array
    return label_data_table


def main(argv=None):
    if argv is None:
        argv = sys.argv

    if len(argv) == 1:
        app = graphical_user_interface.GraphicalUserInterface()
        app.mainloop()
        return 0

    # Parse arguments
    parser = ArgumentParser()
    parser.add_data_argument()
    parser.add_output_file_path_prefix_argument()
    parser.add_sample_information_file_path_argument()
    parser.add_ranges_argument()
    args = parser.parse_input(argv[1:])

    # Get sample information table containing smiles, locations and molecular weight
    sample_information_table = get_sample_information_table(args.sample_information_file_path)

    # Get samples, samples
    # with warnings.catch_warnings():
    #     warnings.filterwarnings("error")
    analyzer = MassSpectraAnalyzer(args.data, sample_information_table, args.retention_time_range, args.mz_range)
    analyzer.process_spectra()

    # Get results table
    results_table = analyzer.get_results_table()

    # Export heatmaps
    mass_spectrometry_heatmap_tool.export_heatmaps(args.output_file, results_table)
    return 0


if __name__ == "__main__":
    sys.exit(main())
