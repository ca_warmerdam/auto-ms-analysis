#!/usr/bin/env python3
"""
Script containing a mass spectra analyzer class responsible for analysing mass spectra in mzXML format
"""

import os
import re
import sys

from sample import Sample

__author__ = "C.A. (Robert) Warmerdam"
__credits__ = ["Robert Warmerdam"]
__version__ = "0.0.2"
__status__ = "Development"


class MassSpectraAnalyzer:
    def __init__(self, data, sample_information_table, retention_time_range, mz_range):
        self.samples = self.get_spectra(data, sample_information_table, retention_time_range, mz_range)
        self.sample_information_table = sample_information_table.copy(deep=True)

    @staticmethod
    def get_spectra(data, sample_information_table, retention_time_range, mz_range):
        """
        Creates samples by matching the data file names with the samples information table data
        The location from the samples information table should be present in a file name
        :param retention_time_range:
        :param mz_range:
        :param data: list of mzXML files
        :param sample_information_table: pandas data frame containing at least the columns 'location' and 'mol_weight'
        :return: list of samples
        """
        samples = list()
        for file in data:

            path = file.name
            filename = os.path.basename(path)

            # Select the row from the samples information table that is present in the filename
            row = sample_information_table[
                [re.search(loc + r'\D', filename) is not None for loc in sample_information_table['location']]]

            # Get molecular weight
            mol_weight = float(row['mol_weight'])

            # Create spectrum instance
            identifier = row['location'].values[0]

            # Add the spectrum if the molecular weight is within the required range
            if mz_range[0] <= mol_weight <= mz_range[1]:
                print("\rImporting samples, file: {}, {}".format(identifier, filename), end='')
                samples.append(Sample(path, identifier, mol_weight, retention_time_range, mz_range))
            else:
                print("{}The mass of the product in well '{}' does not fit in the given mz range '{}-{}'".format(
                    os.linesep,
                    identifier,
                    *mz_range
                ), file=sys.stderr, flush=True)
        print(os.linesep)
        return samples

    def process_spectra(self):
        """
        Process mass spectra samples
        """
        print("Processing spectra...")
        for sample in self.samples:
            sample.get_peak_spectrum()
            sample.process_mass_spectrum()
            sample.get_prediction()

    def get_results_table(self):
        """
        Process results to include the prediction and filter non predicted entries
        :return: sample information numpy table with prediction column
        """
        # Remove not used samples from information table
        ids = [spectrum.identifier for spectrum in self.samples]
        sample_information_table = self.sample_information_table[
            self.sample_information_table['location'].isin(ids)].copy()

        # Write predictions to sample information table
        for index, row in sample_information_table.iterrows():
            sample = list(filter(lambda x: x.identifier == row['location'], self.samples))[0]
            sample_information_table.at[index, 'prediction'] = sample.prediction
        return sample_information_table


def main(argv=None):
    if argv is None:
        argv = sys.argv
    return 0


if __name__ == "__main__":
    sys.exit(main())