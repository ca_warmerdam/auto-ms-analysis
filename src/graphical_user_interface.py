#!/usr/bin/env python3
"""
Script responsible for serving a graphical user interface for analyzing mass spectra.

usage: graphical_user_interface.py
"""

import sys
import tkinter as tk
import warnings
from argparse import ArgumentError

import analyse_mass_spectrometry_data
import mass_spectrometry_heatmap_tool

from argument_parser import ArgumentParser
from mass_spectrum_analyzer import MassSpectraAnalyzer
from sample import Sample
from tkinter import filedialog, messagebox

__author__ = "C.A. (Robert) Warmerdam"
__credits__ = ["Robert Warmerdam"]
__status__ = "Development"


class GraphicalUserInterface(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        container = tk.Frame(self)

        container.pack(side="top", fill="both", expand=True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        frame = MainPage(container, self)

        self.frames[MainPage] = frame

        frame.grid(row=0, column=0, columnspan=3, sticky="nsew", padx=4, pady=4)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


class MainPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.winfo_toplevel().title("Mass spectrum analyzer")

        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.mz_xml_file_paths = list()
        self.smiles_file_path = None
        self.output_file_path = None
        self.retention_time_range = (0, 0)
        self.mz_range = (0, 0)
        self._min_mz_range_var = tk.StringVar()
        self._min_retention_time_var = tk.StringVar()
        self._max_mz_range_var = tk.StringVar()
        self._max_retention_time_var = tk.StringVar()
        self._ion_name = tk.StringVar()
        self._ion_mass_multiplier = tk.StringVar()
        self._ion_mass_addition = tk.StringVar()

        self._mz_xml_files_entry = tk.Entry(self)
        self._smiles_file_entry = tk.Entry(self)
        self._output_file_entry = tk.Entry(self)
        self._min_mz_range_entry = tk.Entry(self, textvariable=self._min_mz_range_var)
        self._max_mz_range_entry = tk.Entry(self, textvariable=self._max_mz_range_var)
        self._min_time_range_entry = tk.Entry(self, textvariable=self._min_retention_time_var)
        self._max_time_range_entry = tk.Entry(self, textvariable=self._max_retention_time_var)
        self._adduct_mass_label_entry = tk.Entry(self, textvariable=self._ion_name)
        self._adduct_mass_multiplication_entry = tk.Entry(self, textvariable=self._ion_mass_multiplier)
        self._adduct_mass_addition_entry = tk.Entry(self, textvariable=self._ion_mass_addition)
        self._frequent_adduct_scroll = tk.Scrollbar(self, orient=tk.VERTICAL)
        self._frequent_adduct_listbox = tk.Listbox(self, yscrollcommand=self._frequent_adduct_scroll.set, height=6)

        self.adduct_entries = Sample.frequent_adduct_masses

        self.create_widgets()

    def create_widgets(self):
        """
        Create widgets
        """
        # Make everything sticky to let the flow in unused space
        # Create and place mz xml files input
        label = tk.Label(self, text="Select mzXML files")
        label.grid(row=1, column=0, columnspan=3, sticky="nsew")
        self._mz_xml_files_entry.grid(row=2, column=0, columnspan=3, sticky="nsew")
        browse_button = tk.Button(self, text="browse", command=self.browse_for_mz_xml_files)
        browse_button.grid(row=2, column=3, columnspan=2, sticky="nsew")

        # Create and place smiles file input
        label = tk.Label(self, text="Select smiles file")
        label.grid(row=3, column=0, columnspan=3, sticky="nsew")
        self._smiles_file_entry.grid(row=4, column=0, columnspan=3, sticky="nsew")
        browse_button = tk.Button(self, text="browse", command=self.browse_for_smiles_file)
        browse_button.grid(row=4, column=3, columnspan=2, sticky="nsew")

        # Create and place output file input
        label = tk.Label(self, text="Enter output file")
        label.grid(row=5, column=0, columnspan=3, sticky="nsew")
        self._output_file_entry.grid(row=6, column=0, columnspan=3, sticky="nsew")
        browse_button = tk.Button(self, text="browse", command=self.browse_for_output_file)
        browse_button.grid(row=6, column=3, columnspan=2, sticky="nsew")

        # Place inputs for m/z range
        label = tk.Label(self, text="M/Z range")
        label.grid(row=7, column=0, columnspan=3, sticky="nsew")
        # Use for sticky only w(west) to maintain constant width
        self._min_mz_range_entry.grid(row=8, columnspan=2, column=0, sticky="w")
        label = tk.Label(self, text="-")
        label.grid(row=8, column=1, sticky="nsew")
        self._max_mz_range_entry.grid(row=8, column=2, sticky="nsew")
        # Insert default values
        self._min_mz_range_entry.insert(1, 200)
        self._max_mz_range_entry.insert(1, 600)

        # Place inputs for time range
        label = tk.Label(self, text="Time range")
        label.grid(row=9, column=0, columnspan=3, sticky="nsew")
        # Use for sticky only w(west) to maintain constant width
        self._min_time_range_entry.grid(row=10, column=0, sticky="w")
        label = tk.Label(self, text="-")
        label.grid(row=10, column=1, sticky="nsew")
        self._max_time_range_entry.grid(row=10, column=2, sticky="e")
        # Place default values
        self._min_time_range_entry.insert(1, 0.2)
        self._max_time_range_entry.insert(1, 0.5)

        # Place inputs for adduct entries
        label = tk.Label(self, text="frequent adduct masses")
        label.grid(row=11, column=0, columnspan=3, sticky="nsew")
        label = tk.Label(self, text="Name")
        label.grid(row=12, column=0, columnspan=2, sticky="w")
        self._adduct_mass_label_entry.grid(row=12, column=2, columnspan=1, sticky="nsew")
        label = tk.Label(self, text="Multiplication factor")
        label.grid(row=13, column=0, columnspan=2, sticky="w")
        self._adduct_mass_multiplication_entry.grid(row=13, column=2, columnspan=1, sticky="nsew")
        label = tk.Label(self, text="Addition of mass")
        label.grid(row=14, column=0, columnspan=2, sticky="w")
        self._adduct_mass_addition_entry.grid(row=14, column=2, columnspan=1, sticky="nsew")

        # Place buttons for adding and removing adduct entries
        addition_button = tk.Button(self, text="Add", command=self.add_adduct_entry)
        addition_button.grid(row=15, column=0, sticky="nsew")
        delete_button = tk.Button(self, text="Delete", command=self.remove_adduct_entry)
        delete_button.grid(row=15, column=2, sticky="nsew")

        # Place list of adduct entries
        self._frequent_adduct_scroll.config(command=self._frequent_adduct_listbox.yview)
        self._frequent_adduct_scroll.grid(row=16, column=3, sticky="nsw")
        self._frequent_adduct_listbox.grid(row=16, column=0, columnspan=3, sticky="nsew")

        # Update adduct entries
        self.update_frequent_adduct_listbox()

        # Place submit button
        submit_button = tk.Button(self, text="Run", command=self.run)
        submit_button.grid(row=17, column=0, columnspan=3, sticky="nsew")

    def run(self):
        """
        Run mass spectrum analysis
        """
        try:
            # Set mz range
            self.set_mz_range()

            # Set retention time range
            self.set_retention_time_range()

            # Get mz data
            data = self.get_mz_data()

            # Get sample information table containing smiles, locations and molecular weight
            sample_information_table = self.get_smiles_data()

            # Get samples, samples
            with warnings.catch_warnings():
                warnings.filterwarnings("error")
                analyzer = MassSpectraAnalyzer(data,
                                               sample_information_table,
                                               self.retention_time_range,
                                               self.mz_range)
                analyzer.process_spectra()

            # Get results table
            results_table = analyzer.get_results_table()

            # Export heatmaps
            mass_spectrometry_heatmap_tool.export_heatmaps(self.output_file_path, results_table)
        except (ValueError, ArgumentError) as e:
            messagebox.showinfo("Error", "{}.".format(str(e)))

    def get_smiles_data(self):
        """
        Get smiles data table
        :return: Smiles data table, ValueError if not entered.
        """
        if len(self.smiles_file_path) < 1:
            raise ValueError("no smiles file entered")

        sample_information_table = analyse_mass_spectrometry_data.get_sample_information_table(
            self.smiles_file_path)
        return sample_information_table

    def get_mz_data(self):
        """
        Get mz data
        :return: Files, ValueError if not entered.
        """
        # Check length
        if len(self.mz_xml_file_paths) < 1:
            raise ValueError("no mzXML files entered")
        # Open data files
        data = [ArgumentParser.is_data(path) for path in self.mz_xml_file_paths]
        return data

    def update_frequent_adduct_listbox(self):
        """
        Updates adduct entries listbox.
        """
        self._frequent_adduct_listbox.delete(0, tk.END)
        for entry in self.adduct_entries:
            self._frequent_adduct_listbox.insert(tk.END, "; ".join([str(value) for value in entry]))

    def browse_for_mz_xml_files(self):
        """
        Browses for multiple files and sets mz xml files entry
        """
        self.mz_xml_file_paths = list(filedialog.askopenfilenames())
        self._mz_xml_files_entry.delete(0, tk.END)
        self._mz_xml_files_entry.insert(1, ";".join(self.mz_xml_file_paths))

    def browse_for_smiles_file(self):
        """
        Browses for a file and sets smiles file entry
        """
        self.smiles_file_path = filedialog.askopenfilename()
        self._smiles_file_entry.insert(1, self.smiles_file_path)

    def browse_for_output_file(self):
        """
        Browses directories for a new file (or for overwriting a file)
        """
        self.output_file_path = filedialog.asksaveasfilename()
        # If the excel extension is not used, add id to the filename
        if self.output_file_path.endswith(".xlsx") is False:
            self.output_file_path += ".xlsx"
        self._output_file_entry.insert(1, self.output_file_path)

    def add_adduct_entry(self):
        """
        Adds adduct entries
        :return: 1 if ValueErrors were raised while an adduct entry.
        """
        # Get values
        ion_name_value = self._ion_name.get()
        ion_mass_multiplier_value = self._ion_mass_multiplier.get()
        ion_mass_addition_value = self._ion_mass_addition.get()

        try:
            # Check if ion name has no length
            if len(ion_name_value) == 0:
                # Alert user
                raise ValueError("Ion name not entered")

            # Check if multiplier can be converted to a float
            try:
                ion_mass_multiplier_float_value = self.get_value(ion_mass_multiplier_value)
            except ValueError as e:
                # Alert user
                raise ValueError("Ion mass multiplier {}".format(str(e)))

            # Check if addition value length and if it can be converted to a float
            try:
                ion_mass_addition_float_value = self.get_value(ion_mass_addition_value)
            except ValueError as e:
                # Alert user
                raise ValueError("Ion mass addition {}".format(str(e)))
        except ValueError as e:
            messagebox.showinfo("Error", "{}.".format(str(e)))
            return 1

        self.adduct_entries.append((ion_name_value,
                                    ion_mass_multiplier_float_value,
                                    ion_mass_addition_float_value))
        self.update_frequent_adduct_listbox()

    def remove_adduct_entry(self):
        """
        Removes an adduct entry at the selected index of the adduct entry listbox
        """
        selected_items = self._frequent_adduct_listbox.curselection()
        for index in selected_items:
            del self.adduct_entries[index]
        self.update_frequent_adduct_listbox()

    def set_mz_range(self):
        """
        Sets the M/Z range from the min and max entries
        """
        # Get min time value
        try:
            min_mz_range_float_value = self.get_value(self._min_mz_range_var.get())
        except ValueError as e:
            raise ValueError("M/Z range minimum {}".format(str(e)))

        # Get max time value
        try:
            max_mz_range_float_value = self.get_value(self._max_mz_range_var.get())
        except ValueError as e:
            raise ValueError("M/Z range maximum {}".format(str(e)))

        # Set value
        self.mz_range = (min_mz_range_float_value, max_mz_range_float_value)

    def set_retention_time_range(self):
        """
        Sets the retention time range from the min and max entries
        """
        # Get min time value
        try:
            min_time_range_float_value = self.get_value(self._min_retention_time_var.get())
        except ValueError as e:
            raise ValueError("Retention time range minimum {}".format(str(e)))

        # Get max time value
        try:
            max_time_range_float_value = self.get_value(self._max_retention_time_var.get())
        except ValueError as e:
            raise ValueError("Retention time range maximum {}".format(str(e)))

        self.retention_time_range = (min_time_range_float_value, max_time_range_float_value)

    @staticmethod
    def get_value(value):
        """
        Converts value to float if it has length.
        Raises ValueError if either the length was not more than 0 or if the value could not be
        converted to float.
        :param value: Value to convert to float
        :return: The converted value
        """
        # Check if min mz range has no length
        if len(value) == 0:
            # Alert user
            raise ValueError("not entered")

        # Check if min mz range can be converted to a float
        try:
            return float(value)
        except ValueError as e:
            raise ValueError("is not a numeric value ('{}')".format(value))


def main(argv=None):
    if argv is None:
        argv = sys.argv
    app = GraphicalUserInterface()
    app.mainloop()
    return 0


if __name__ == "__main__":
    sys.exit(main())
