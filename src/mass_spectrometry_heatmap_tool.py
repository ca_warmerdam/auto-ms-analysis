#!/usr/bin/env python3
"""
usage: python mass_spectrometry_heatmap_tool.py [-h] -l LABEL_DATA -o OUTPUT_FILE -i
                                                        SAMPLE_INFORMATION_FILE_PATH

Automatic mass spectrometry analyzer

optional arguments:
  -h, --help            show this help message and exit
  -l LABEL_DATA, --label_data LABEL_DATA
                        Give a file with locations in the first column and
                        labels (0, 0.5, 1) in the second column.
  -o OUTPUT_FILE, --output_file OUTPUT_FILE
                        Give a output file path the output file can be written
                        to. For example: 'd:\files\heatmap.xlsx'
  -i SAMPLE_INFORMATION_FILE_PATH, --sample_information_file_path SAMPLE_INFORMATION_FILE_PATH
                        Specify a smiles file with spaces as a separator that
                        contains a smiles in the first column and the location
                        on the output plate in the last column.
"""

import re
import sys
import analyse_mass_spectrometry_data

import xlsxwriter as xlsxwriter

from xlsxwriter.utility import xl_rowcol_to_cell
from argument_parser import ArgumentParser

__author__ = "C.A. (Robert) Warmerdam"
__credits__ = ["Robert Warmerdam"]
__status__ = "Development"


def add_compact_heatmap_worksheet(sample_information_table, workbook):
    """
    Adds a worksheet with a compact heatmap
    :param sample_information_table:
    :param workbook:
    """
    # Add the worksheet
    worksheet = workbook.add_worksheet('compact-heatmap')

    # Start from the first cell. Rows and columns are zero indexed.
    row = 0
    col = 0

    # Define format for prediction cells
    cell_format = workbook.add_format()
    cell_format.set_align('center')

    # Iterate through rows of information table
    letters = set()
    numbers = set()
    for index, sample_row in sample_information_table.iterrows():
        loc = sample_row['location']
        prediction = sample_row['prediction']

        # Get letters and numbers from identifier
        match = re.match(r"([A-Z]+)([0-9]+)", loc, re.I)

        if match:
            letter = ord(match.groups()[0]) - 64
            letters.add(letter)
            number = int(match.groups()[1])
            numbers.add(number)
            worksheet.write(letter, number, round(prediction, 1), cell_format)

    # Set header cell format
    header_cell_format = workbook.add_format()
    header_cell_format.set_align('center')
    header_cell_format.set_bold()

    # Write column and row names
    worksheet.write_row(row, col + 1, list(range(1, max(numbers) + 1)), header_cell_format)
    worksheet.write_column(row + 1, col, [chr(i + 64) for i in range(1, max(letters) + 1)], header_cell_format)

    # Add color scale
    cell_ranges = '{}:{}'.format(xl_rowcol_to_cell(1, 1), xl_rowcol_to_cell(max(letters), max(numbers)))
    worksheet.conditional_format(cell_ranges,
                                 {'type': '3_color_scale'})

    # Set column widths
    worksheet.set_column(0, max(numbers), 3.57)
    return 0


def add_smiles_heatmap_worksheet(sample_information_table, workbook):
    # Add the worksheet
    worksheet = workbook.add_worksheet('smiles-heatmap')

    # Set cell size
    worksheet.set_default_row(41)
    worksheet.set_row(0, 20)

    # Start from the first cell. Rows and columns are zero indexed.
    row = 0
    col = 0

    # Iterate through rows of information table
    letters = set()
    numbers = set()

    # Iterate through rows of information table
    for index, sample_row in sample_information_table.iterrows():
        loc = sample_row['location']
        smiles_structure = sample_row['smiles']
        prediction = sample_row['prediction']

        # Get letters and numbers from identifier
        match = re.match(r"([A-Z]+)([0-9]+)", loc, re.I)

        if match:
            letter = ord(match.groups()[0]) - 64
            letters.add(letter)
            number = int(match.groups()[1])
            numbers.add(number)
            worksheet.write(number, letter, smiles_structure)
            worksheet.write(number, letter + 26, prediction)

    # Set header cell format
    header_cell_format = workbook.add_format()
    header_cell_format.set_align('center')

    # Write column and row names
    worksheet.write_column(row + 1, col, list(range(1, max(numbers) + 1)), header_cell_format)
    worksheet.write_row(row, col + 1, [chr(i + 64) for i in range(1, max(letters) + 1)], header_cell_format)

    # Add color scale
    # 1 to indicate the second row and col, 26 to shift it with the length of the alphabet across columns
    cell_ranges = '{}:{}'.format(xl_rowcol_to_cell(1, 1+26), xl_rowcol_to_cell(max(numbers), max(letters)+26))
    worksheet.conditional_format(cell_ranges,
                                 {'type': '3_color_scale'})

    # Set cell size
    worksheet.set_column(0, 0, 3.14)
    worksheet.set_column(1, max(letters), 16)

    return 0


def add_sample_information_table_worksheet(sample_information_table, workbook):
    # Add the worksheet
    worksheet = workbook.add_worksheet('table')

    # Iterate through rows of information table
    for index, sample_row in sample_information_table.iterrows():
        # Write row to the worksheet
        row_list = list(sample_row)
        worksheet.write_row(index, 0, row_list)

    return 0


def add_specific_table_worksheet(sample_information_table, worksheet, label):
    # Set cell size
    worksheet.set_default_row(112.5)

    # Start at the origin of the worksheet
    row = 0
    column = 0

    # Iterate through rows of information table
    for index, sample_row in sample_information_table.iterrows():
        loc = sample_row['location']
        smiles_structure = sample_row['smiles']
        prediction = sample_row['prediction']

        if prediction == label:
            worksheet.write(row, column, smiles_structure)

            # Set following cell
            column += 1
            if column == 4:
                column = 0
                row += 1

    # Set cell size
    worksheet.set_column(0, 4, 20)

    return 0


def update_sample_information_table(label_data_table, sample_information_table):
    """
    Extends the sample information table with label data.
    :param label_data_table: The table with sample locations and labels.
    :param sample_information_table: The table with sample locations and smiles strings.
    :return:
    """
    for index, row in sample_information_table.iterrows():
        label_row = list(filter(lambda x: x[1]['location'] == row['location'], label_data_table.iterrows()))
        if len(label_row) == 1:
            label_row = label_row[0][1]
            if not label_row['label'] in ('0', '0.5', '1'):
                print("Sample '{}' does not have a valid label!".format(label_row['location']), file=sys.stderr)
                continue
        else:
            print("Sample '{}' should only be present a single time in the label file".format(row['location']),
                  file=sys.stderr)
            continue
        sample_information_table.at[index, 'prediction'] = float(label_row['label'])
    # Filter sample information table
    sample_information_table = sample_information_table[sample_information_table['prediction'].isin([float(0),
                                                                                                     float(0.5),
                                                                                                     float(1)])]
    return sample_information_table


def export_heatmaps(output_file, sample_information_table):
    """
    Function that export heatmaps in an excel file.
    :param output_file: The excel file name
    :param sample_information_table: The table that contains information about locations, labels and structure.
    """
    # Create a workbook
    print("Exporting heatmaps...")
    workbook = xlsxwriter.Workbook(output_file)
    # Add the compact worksheet
    add_compact_heatmap_worksheet(sample_information_table, workbook)
    # Add the smiles heatmap
    add_smiles_heatmap_worksheet(sample_information_table, workbook)
    # Add the sample information table
    add_sample_information_table_worksheet(sample_information_table, workbook)
    # Add the green table
    green = workbook.add_worksheet('green')
    add_specific_table_worksheet(sample_information_table, green, 1)
    green = workbook.add_worksheet('yellow')
    add_specific_table_worksheet(sample_information_table, green, 0.5)
    # Close and exit
    workbook.close()
    print("Done!")


def main(argv=None):
    if argv is None:
        argv = sys.argv

    # Parse arguments
    parser = ArgumentParser()
    parser.add_label_data_argument()
    parser.add_output_file_path_prefix_argument()
    parser.add_sample_information_file_path_argument()
    args = parser.parse_input(argv[1:])

    # Get sample information table containing smiles, locations and molecular weight
    sample_information_table = analyse_mass_spectrometry_data.get_sample_information_table(
        args.sample_information_file_path)

    # Remove not used samples from information table
    # ids = [spectrum.identifier for spectrum in args.args.label_data]
    # sample_information_table = sample_information_table[sample_information_table['location'].isin(ids)]
    label_data_table = analyse_mass_spectrometry_data.get_label_data_table(
        args.label_data)

    # Write predictions to sample information table
    sample_information_table = update_sample_information_table(label_data_table, sample_information_table)
    # Export heatmaps
    export_heatmaps(args.output_file, sample_information_table)
    return 0


if __name__ == "__main__":
    sys.exit(main())