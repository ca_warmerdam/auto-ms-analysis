#!/usr/bin/env python3

"""
Script containing the definition of an argument parser built with the argparse module
"""

import glob
import sys
import os
import argparse

__author__ = "C.A. (Robert) Warmerdam"
__credits__ = ["Robert Warmerdam"]
__version__ = "0.0.2"
__status__ = "Development"


class ArgumentParser:
    def __init__(self):
        self.parser = self.create_argument_parser()

    def parse_input(self, argv):
        """
        Parse command line input.
        :param argv: given arguments
        :return: parsed arguments
        """
        args = self.parser.parse_args(argv)
        # Compare list sizes if there is training data
        if hasattr(args, 'training_data'):
            if len(args.training_data) != len(args.sample_information_file_paths):
                raise argparse.ArgumentTypeError("Did not receive the correct amount of sample information file paths. "
                                                 "Should be equal to training data argument count")
            if len(args.training_data) != len(args.label_data):
                raise argparse.ArgumentTypeError("Did not receive the correct amount of label data file paths. "
                                                 "Should be equal to training data argument count")

        # Flatten list if there is training data
        if hasattr(args, 'data'):
            args.data = [item for sublist in args.data for item in sublist]

        # Only pass the name of the output file prefix
        if hasattr(args, 'output_file_path_prefix'):
            args.output_file_path_prefix = args.output_file_path_prefix.name

        # Only pass the name of the output model
        if hasattr(args, 'model_path'):
            args.model_path = args.model_path.name

        # Check retention time range
        if hasattr(args, 'retention_time_range') and args.retention_time_range[0] >= args.retention_time_range[1]:
            raise argparse.ArgumentTypeError("The first value should be lower than the last value")

        # Check mass-charge range
        if hasattr(args, 'mz_range') and args.mz_range[0] >= args.mz_range[1]:
            raise argparse.ArgumentTypeError("The first value should be lower than the last value")

        return args

    def create_argument_parser(self):
        """
        Method creating an argument parser
        :return: parser
        """
        parser = argparse.ArgumentParser(description="Automatic mass spectrometry analyzer",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        return parser

    def add_sample_information_file_path_argument(self):
        self.parser.add_argument('-i', '--sample_information_file_path', type=self.is_readable_file,
                                 required=True,
                                 default=None,
                                 help="Specify a smiles file with spaces as a separator that contains a smiles in the "
                                      "first column and the location on the output plate in the last column.")

    def add_data_argument(self):
        self.parser.add_argument('-d', '--data', type=self.is_data, required=True, nargs='+', default=None,
                                 help="Give a path for the directory where data is stored to analyse. "
                                      "{0}This can be multiple files. The asterisk can be used as wildcard. "
                                      "{0}\tThe argument 'd:\\files\\*.mzxml'"
                                      "{0}\twill result in all mzxml files being analyzed located inside the folder:"
                                      "{0}\t'd:\\files'".format(os.linesep))

    def add_label_data_argument(self):
        self.parser.add_argument('-l', '--label_data', type=argparse.FileType('r'), required=True, default=None,
                                 help="Give a file with locations in the first column "
                                      "and labels (0, 0.5, 1) in the second column.")

    def add_output_file_path_prefix_argument(self):
        self.parser.add_argument('-o', '--output_file', type=argparse.FileType('wb'),
                                 required=True, default=None,
                                 help="Give a file path the output files can be written to. "
                                      "For example: 'd:\\files\\heatmap.xlsx'")

    def add_ranges_argument(self):
        self.parser.add_argument('-R', '--retention_time_range', type=self.is_positive_float, required=False,
                                 default=[0.2, 0.5],
                                 nargs=2,
                                 help="Specify the retention time to filter on. default is between 0.2 and 0.5.")
        self.parser.add_argument('-M', '--mz_range', type=self.is_positive_float, required=False,
                                 default=[200.0, 600.0],
                                 nargs=2,
                                 help="Specify the mass/charge range to filter on. default is between 200 and 600.")

    @staticmethod
    def is_positive_float(value):
        """
        Checks whether a
        :param value: the string to check.
        :return: a checked float value.
        """
        float_value = float(value)
        if float_value <= 0:
            raise argparse.ArgumentTypeError("{} is an invalid float value (must be above 0.0)".format(value))
        return float_value

    @staticmethod
    def is_readable_dir(train_directory):
        """
        Checks whether the given directory is readable
        :param train_directory: a path to a directory in string format
        :return: train_directory
        :raises: Exception: if the given path is invalid
        :raises: Exception: if the given directory is not accessible
        """
        if not os.path.isdir(train_directory):
            raise Exception("directory: {0} is not a valid path".format(train_directory))
        if os.access(train_directory, os.R_OK):
            return train_directory
        else:
            raise Exception("directory: {0} is not a readable dir".format(train_directory))

    @staticmethod
    def is_readable_file(file_path):
        """
        Checks whether the given directory is readable
        :param file_path: a path to a file in string format
        :return: file_path
        :raises: Exception: if the given path is invalid
        :raises: Exception: if the given directory is not accessible
        """
        if not os.path.isfile(file_path):
            raise Exception("file path:{0} is not a valid file path".format(file_path))
        if os.access(file_path, os.R_OK):
            return file_path
        else:
            raise Exception("file path:{0} is not a readable file".format(file_path))

    @staticmethod
    def is_data(argument):
        """
        Parses a string that specifies the path(s) of the data.
        :param argument:
        :return:
        """
        # If an asterisk is the string, expand using glob.
        if '*' in argument:
            paths = glob.glob(argument)
            files = []
            for file_path in paths:
                files.append(argparse.FileType('r')(file_path))
            if len(files) == 0:
                raise argparse.ArgumentTypeError(argument + ' matches exactly zero files.')
            return files
        else:
            # Use default argparse filename parser if there is not an asterisk in the string.
            return argparse.FileType('r')(argument)

    def is_writable_location(self, path):
        self.is_readable_dir(os.path.dirname(path))
        return path


def main(argv=None):
    if argv is None:
        argv = sys.argv
    return 0


if __name__ == "__main__":
    sys.exit(main())
