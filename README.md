# Script for classifying mass spectrometry mzXML files.

This is a script for automatic classification of mass spectrometry mzXML files

## Getting started

Before you can run the program, make sure that the requirements are met.
The program requires python and open babel, both in 32-bit, or both in 64-bit.

* Install python (tested with version 3.6.6):

    * [64-bit installer download](https://www.python.org/ftp/python/3.6.6/python-3.6.6-amd64.exe)
    * [32-bit installer download](https://www.python.org/ftp/python/3.6.6/python-3.6.6.exe)
    * Under 'advanced options' make sure to enable/check 'Add Python to environment variables'

* Install the [Open Babel](http://openbabel.org/wiki/Main_Page) program. (Make sure that you pick the 64-bit version if your python version is also 64-bit, otherwise pick 32-bit)

    * [64-bit installer download (version 3.0.0)](https://github.com/openbabel/openbabel/releases/download/openbabel-3-0-0/OpenBabel-3.0.0.exe)
    * [32-bit installer download (version 3.0.0)](https://github.com/openbabel/openbabel/releases/download/openbabel-3-0-0/OpenBabel-3.0.0-x86.exe)
    
* If another version of Open Babel was installed than version 3.0.0, open the `requirements.txt` file and make sure the version number of `openbabel-python` matches the version of your Open Babel installation.

* Download this [program](https://bitbucket.org/ca_warmerdam/auto-ms-analysis/get/master.zip) and extract it to a location you like

* Open command prompt and change directory to this programs directory, where the `requirements.txt` file is located

* type `pip install -r requirements.txt`

## Usage

To start the graphical user interface, type the following while in a command prompt in the `src` directory
```
python analyse_mass_spectrometry_data.py
```

The command line script can be used as follows:
```
python analyse_mass_spectrometry_data.py [-h] -d DATA [DATA ...] -o
                                         OUTPUT_FILE -i
                                         SAMPLE_INFORMATION_FILE_PATH
                                         [-R RETENTION_TIME_RANGE RETENTION_TIME_RANGE]
                                         [-M MZ_RANGE MZ_RANGE]

Automatic mass spectrometry analyzer

optional arguments:
  -h, --help            show this help message and exit
  -d DATA [DATA ...], --data DATA [DATA ...]
                        Give a path for the directory where data is stored to
                        analyse. This can be multiple files. The asterisk can
                        be used as wildcard. The argument 'd:\files\*.mzxml'
                        will result in all mzxml files being analyzed located
                        inside the folder: 'd:\files'
  -o OUTPUT_FILE, --output_file OUTPUT_FILE
                        Give a file path the output files can be written to.
                        For example: 'd:\files\heatmap.xlsx'
  -i SAMPLE_INFORMATION_FILE_PATH, --sample_information_file_path SAMPLE_INFORMATION_FILE_PATH
                        Specify a smiles file with spaces as a separator that
                        contains a smiles in the first column and the location
                        on the output plate in the last column.
  -R RETENTION_TIME_RANGE RETENTION_TIME_RANGE, --retention_time_range RETENTION_TIME_RANGE RETENTION_TIME_RANGE
                        Specify the retention time to filter on. default is
                        between 0.2 and 0.5.
  -M MZ_RANGE MZ_RANGE, --mz_range MZ_RANGE MZ_RANGE
                        Specify the mass/charge range to filter on. default is
                        between 200 and 600.
```

### Creation of mzXML files

mzXML files can be created with multiple programs. This script has been tested with
the `MSConvert` tool from the [ProteoWizard](http://proteowizard.sourceforge.net/) project.
Download and install this as is instructed on the website. Once completed the `MSConvert`
tool should be available on your computer. Start this program.

Now convert the `.raw` files to `.mzXML` files by selecting the files and setting the correct
extension 'mzXML'. Keep the default settings. At the time of writing this, this means the following:

* 64 bit binary encoding precision (important!)
* Write index is checked
* TPP compatibility is checked
* Use zlib compression is checked (important!)
* No further checks or filters

Select the output directory and hit `start`, your files will be created

### Sample information file

The sample information file should at least have a smiles molecule in the first
column and a sample location in the last column. Make sure the data from this sample
information file corresponds to the mxXML files givenAn example:
```
CSCCC1=C(NCCCOC(C)C)N2C=CC(Cl)=CC2=N1 A8 J1 I14 A1
CCCCCNC1=CN=C2N1C=CC=C2CO C8 G4 E15 A2
CCCCCNC1=C(CCCC)N=C2SC=CN12 A9 G1 E15 A3
...
```

### Colored smiles structures

The output file holds a couple of heatmaps, the second of which contains SMILES.
these SMILES can be converted to structures using an excel add-in. One add-in that can
be used is [JChem for Office](https://chemaxon.com/products/jchem-for-office). To use
this, download and install it first. After that is complete, in excel, switch the JChem tab to the to the extended
or advanced menu set. Subsequently, the 'from SMILES' button can be used to convert the selected
cells to structures. A limitation in Microsoft Excel is that the cells that contain the SMILES structures
cannot also be conditionally formatted with a color gradient by other values in the sheet.
Due to this limitation a trick is required to color the cells according to a user-definable gradient.
The gradient can be altered in the colored cells in the same sheet, which then can be copied to the structure cells.

To copy the gradient colors:

* Press the key combination `Alt+F11` in Excel
* Double-click on the correct sheet
* Set your cursor in the textbox that has appeared
* Paste the following code:

```
Sub copyColors()
    Dim rFirst As Range
    Dim rSecond As Range
    Set rFirst = Me.Range("B2:Q25")
    Set rSecond = Me.Range("AB2:AQ25")
    Dim iLoop As Integer
    For iLoop = 1 To rFirst.Count
        rFirst.Cells(iLoop).Interior.Color = rSecond.Cells(iLoop).DisplayFormat.Interior.Color
    Next iLoop
End Sub
```

* press F5
* close the 'Microsoft Visual Basic for Applications' window

### Structures into word

Copying the structure tables from Excel to Word can be done by first copying a selection of cells normally,
then placing the cursor in word at the bottom of the table, and using the copy and paste functionality from
the JChem tab to paste the structures into the cells in Word.

Because the structures in Microsoft Word appear as pictures, editing the table in Word proved to be
challenging. To solve this problem another VBA procedure was developed that changes the text
wrapping of the pictures to the 'tight' wrapping method. I would recommend to be careful with this
because the other pictures with 'inline' wrapping in the Word document also will receive 'tight' wrapping.

To do this:

* Press the key combination `Alt+F11` in Word
* Double-click on the correct sheet
* Set your cursor in the textbox that has appeared
* Paste the following code:

```
Sub tightPictures()
'
' tightPictures Macro
'
'
Dim intCount As Integer
Dim i As Integer
'loop through inline shapes
For Each oILShp In ActiveDocument.InlineShapes
    oILShp.ConvertToShape
    If oILShp.Type = msoPicture Then
        'check the border color to black
        oILShp.WrapFormat.Type = pbWrapTypeTight
    End If
Next
End Sub
```

* press the run button (green arrow)
* close the 'Microsoft Visual Basic for Applications' window
